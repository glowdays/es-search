package app.service.search.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonUtil {

  /**
   * map -> json
   */
  public static JSONObject convertMapToJson(Map<String, Object> map) {
    JSONObject json = new JSONObject();
    if (map == null) {
      return null;
    }
    for (Map.Entry<String, Object> entry : map.entrySet()) {
      json.put(entry.getKey(), entry.getValue());
    }
    return json;
  }

  /**
   * map -> json -> Base64 encode
   */
  public static String convertMapToJson_B64(Map<String, Object> map) {
    return new String(Base64.getEncoder().encode(convertMapToJson(map).toString().getBytes()));
  }

  /**
   * [json_encode] like php method
   * map -> json -> ByteBuffer
   */
  public static ByteBuffer convertMap2ByteBuffer(Map<String, Object> map) {
    if (convertMapToJson(map) == null || convertMapToJson(map).isEmpty()) {
      return convertStr2ByteBuffer("false");
    }

    return convertStr2ByteBuffer(convertMapToJson(map).toString());
  }

  /**
   * listMap -> json
   */
  public static JSONArray convertListToJson(List<Map<String, Object>> listMap) {
    JSONArray jsonArray = new JSONArray();
    if (listMap == null ) {
      return null;
    }
    for (Map<String, Object> map : listMap) {
      jsonArray.add(convertMapToJson(map));
    }
    return jsonArray;
  }

  /**
   * listMap -> json -> Base64 endcode
   */
  public static String convertListToJson_B64(List<Map<String, Object>> listMap) {
    return new String(Base64.getEncoder().encode(convertListToJson(listMap).toString().getBytes()));
  }

  /**
   * listMap -> json -> ByteBuffer
   */
  public static ByteBuffer convertList2ByteBuffer(List<Map<String, Object>> listMap) {
    return convertStr2ByteBuffer(convertListToJson(listMap).toString());
  }

  /**
   * String -> ByteBuffer
   */
  public static ByteBuffer convertStr2ByteBuffer(String str) {
    Charset charset = Charset.forName("UTF-8");
    if (str == null) {
      return charset.encode("null");
    }
    return charset.encode(str);
  }

  /**
   * ByteBuffer -> String
   */
  public static String covertByteBuffer2Str(ByteBuffer byteBuffer) {
    Charset charset = Charset.forName("UTF-8");
    return charset.decode(byteBuffer).toString();
  }

  /**
   * dto -> jsonString
   */
  public static String convertDtoToJsonString(Object object) {
    ObjectMapper objectMapper = new ObjectMapper();
    String jsonStr = "";
    try {
      jsonStr = objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return jsonStr;
  }

  /**
   * dto -> jsonString -> Base64 encode
   */
  public static String convertDtoToJson_B64(Object object) {
    return new String(Base64.getEncoder().encode(convertDtoToJsonString(object).getBytes()));
  }

  /**
   * string -> json
   */
  public static JSONObject convertStr2Json(String str) {
    JSONObject jsonObject = new JSONObject();
    try {
      JSONParser jsonParser = new JSONParser();
      jsonObject = (JSONObject) jsonParser.parse(str);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return jsonObject;
  }

  /**
   * dto -> map
   */
  public static Map<String, Object> convertDto2Map(Object object) {
    Field[] fields = object.getClass().getDeclaredFields();
    Map<String, Object> result = new HashMap<String, Object>();
    try {
      for (int i = 0; i <= fields.length; i++) {
        fields[i].setAccessible(true);
        result.put(fields[i].getName(), fields[i].get(object));
      }
      return result;
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }

    return null;
  }
}
