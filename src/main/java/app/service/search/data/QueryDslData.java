package app.service.search.data;

import glowpick.data.entity.userglowmee.QBrand;
import glowpick.data.entity.userglowmee.QCategorymappingV2;
import glowpick.data.entity.userglowmee.QFirstcategoryV2;
import glowpick.data.entity.userglowmee.QProduct;
import glowpick.data.entity.userglowmee.QProductGoods;
import glowpick.data.entity.userglowmee.QProductScoreSnapshot;
import glowpick.data.entity.userglowmee.QRegister;
import glowpick.data.entity.userglowmee.QReviewcomment;
import glowpick.data.entity.userglowmee.QSecondcategoryV2;
import glowpick.data.entity.userglowmee.QThirdcategoryV2;

public interface QueryDslData {

	QProduct qProduct = QProduct.product;
	QBrand qBrand = QBrand.brand;

	QFirstcategoryV2 qFirstcategoryV2 = QFirstcategoryV2.firstcategoryV2;
	QSecondcategoryV2 qSecondcategoryV2 = QSecondcategoryV2.secondcategoryV2;
	QThirdcategoryV2 qThirdcategoryV2 = QThirdcategoryV2.thirdcategoryV2;
	QCategorymappingV2 qCategorymappingV2 = QCategorymappingV2.categorymappingV2;

	QProductScoreSnapshot qProductScoreSnapshot = QProductScoreSnapshot.productScoreSnapshot;

	QProductGoods qProductGoods = QProductGoods.productGoods;

	QReviewcomment qReviewcomment = QReviewcomment.reviewcomment;
	QRegister qRegister = QRegister.register;

}
