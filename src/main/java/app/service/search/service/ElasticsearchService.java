package app.service.search.service;

import app.service.search.dto.RankingSearchFilterDTO;
import java.util.Map;

public interface ElasticsearchService {

  Map<String, Object> patchProduct(Long id);

  Map<String, Object> patchReview(Long id);

  Map<String, Object> getProduct(Long id);

  Map<String, Object> getReview(Long id);

}
