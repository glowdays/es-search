package app.service.search.service;

import app.service.search.dto.RankingSearchFilterDTO;
import java.util.Map;

public interface RankingService {

  Map<String, Object> getCategoryRanking(Long level, Long id, RankingSearchFilterDTO dto);

}
