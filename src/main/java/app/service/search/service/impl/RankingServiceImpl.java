package app.service.search.service.impl;

import app.service.search.data.QueryDslData;
import app.service.search.dto.RankingSearchFilterDTO;
import app.service.search.service.RankingService;
import java.util.Map;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RankingServiceImpl implements RankingService, QueryDslData {

  @Value("${es.index.product}")
  private String ES_PRODUCT_INDEX;
  @Value("${es.index.review}")
  private String ES_REVIEW_INDEX;

  @Autowired
  RestHighLevelClient restHighLevelClient;

  @Override
  public Map<String, Object> getCategoryRanking(Long level, Long id, RankingSearchFilterDTO dto) {
    SearchRequest searchRequest = new SearchRequest(ES_PRODUCT_INDEX);


    return null;
  }
}
