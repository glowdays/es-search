package app.service.search.service.impl;

import app.service.search.constants.Constants;
import app.service.search.data.QueryDslData;
import app.service.search.dto.ProductDTO;
import app.service.search.dto.ReviewDTO;
import app.service.search.dto.SearchProductDTO;
import app.service.search.service.ElasticsearchService;
import app.service.search.util.JsonUtil;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Qualifier
public class ElasticsearchServiceImpl implements ElasticsearchService, QueryDslData {

  @Value("${es.index.product}")
  private String ES_PRODUCT_INDEX;
  @Value("${es.index.review}")
  private String ES_REVIEW_INDEX;

  @Autowired
  private JPAQueryFactory jpaQueryFactory;

  @Autowired
  RestHighLevelClient restHighLevelClient;

  @Override
  public Map<String, Object> patchProduct(Long id) {

    ProductDTO product = jpaQueryFactory.select(Projections.bean(ProductDTO.class, qBrand.idBrand, qBrand.brandTitle, qBrand.isDisplay.as("brandIsDisplay"),
        Expressions.stringTemplate("group_concat({0})", qSecondcategoryV2.firstcategoryV2.idFirstCategory).as("firstCategories"),
        Expressions.stringTemplate("group_concat({0})", qCategorymappingV2.categorymappingId.idSecondCategory).as("secondCategories"),
        Expressions.stringTemplate("group_concat({0})", qCategorymappingV2.categorymappingId.idThirdCategory).as("thirdCategories"), qProductGoods.goodsCount.as("goods_count"),
        qProductGoods.minPrice.as("min_price"), qProductGoods.maxPrice.as("max_price"), qProduct.idProduct, qProduct.isDisplay, qProduct.productTitle, qProduct.price, qProduct.productScore,
        qProduct.ratingAvg)).from(qProduct).join(qBrand).on(qProduct.brand.idBrand.eq(qBrand.idBrand)).leftJoin(qCategorymappingV2)
        .on(qProduct.idProduct.eq(qCategorymappingV2.categorymappingId.idProduct)).leftJoin(qSecondcategoryV2)
        .on(qCategorymappingV2.categorymappingId.idSecondCategory.eq(qSecondcategoryV2.idSecondCategory)).leftJoin(qProductGoods).on(qProduct.idProduct.eq(qProductGoods.product.idProduct))
        .groupBy(qProduct.idProduct).having(qProduct.idProduct.eq(id)).fetchFirst();

    Map<String, Object> result = new HashMap<>();

    result.put("data", product);

    SearchProductDTO searchProduct = new SearchProductDTO();
    searchProduct.setSearchProductDTO(product);

    UpdateRequest updateRequest = new UpdateRequest(ES_PRODUCT_INDEX, Constants.ES_TYPE, id.toString());
    updateRequest.doc(JsonUtil.convertDtoToJsonString(searchProduct), XContentType.JSON);
    updateRequest.docAsUpsert(true);

//    restHighLevelClient.updateAsync(updateRequest, RequestOptions.DEFAULT, EsActionListener.getUpdateListener());

    UpdateResponse updateResponse = null;
    try {
      updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public Map<String, Object> patchReview(Long id) {

    ReviewDTO review = jpaQueryFactory.select(Projections
        .bean(ReviewDTO.class, qReviewcomment.idreviewcomment, qReviewcomment.product.idProduct, qReviewcomment.register.idRegister, qReviewcomment.isDisplay, qReviewcomment.register.birthYear,
            qReviewcomment.isEvaluation, qReviewcomment.register.isBlind, qReviewcomment.likeCount, qReviewcomment.register.nickName, qReviewcomment.rating, qReviewcomment.register.skinType,
            qReviewcomment.tag)).from(qReviewcomment).innerJoin(qRegister).on(qReviewcomment.register.idRegister.eq(qRegister.idRegister)).where(qReviewcomment.idreviewcomment.eq(id)).fetchFirst();

    Map<String, Object> result = new HashMap<>();

    result.put("data", review);

    UpdateRequest updateRequest = new UpdateRequest(ES_REVIEW_INDEX, Constants.ES_TYPE, id.toString());
    updateRequest.doc(JsonUtil.convertDtoToJsonString(review.toString()), XContentType.JSON);
    updateRequest.docAsUpsert(true);

    UpdateResponse updateResponse = null;
    try {
      updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public Map<String, Object> getProduct(Long id) {
    GetRequest request = new GetRequest(ES_PRODUCT_INDEX).id(id.toString());
    GetResponse getResponse = null;
    try {
      getResponse = restHighLevelClient.get(request, RequestOptions.DEFAULT);
    } catch (IOException e) {
      e.printStackTrace();
    }

    Map<String, Object> result = new HashMap<>();
    result.put("data", getResponse.getSourceAsMap());
    return result;
  }

  @Override
  public Map<String, Object> getReview(Long id) {
    GetRequest request = new GetRequest(ES_REVIEW_INDEX).id(id.toString());
    GetResponse getResponse = null;
    try {
      getResponse = restHighLevelClient.get(request, RequestOptions.DEFAULT);
    } catch (IOException e) {
      e.printStackTrace();
    }

    Map<String, Object> result = new HashMap<>();
    System.out.println(getResponse.getSourceAsMap());
    result.put("data", getResponse.getSourceAsMap());
    return result;
  }
}
