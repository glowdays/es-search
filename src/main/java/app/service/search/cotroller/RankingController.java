package app.service.search.cotroller;

import app.service.search.dto.RankingSearchFilterDTO;
import app.service.search.handler.ResultHandler;
import app.service.search.service.ElasticsearchService;
import app.service.search.service.RankingService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/ranking")
public class RankingController {

  @Autowired
  RankingService rankingService;

  @GetMapping("/category/{level}/{id}")
  public ResponseEntity<?> getCategoryRanking(@PathVariable("level") Long level, @PathVariable("id") Long id, RankingSearchFilterDTO dto) {

    Map<String, Object> result = rankingService.getCategoryRanking(level,id,dto);

    return new ResultHandler().handle(result);
  }
}
