package app.service.search.cotroller;

import app.service.search.handler.ResultHandler;
import app.service.search.service.ElasticsearchService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/doc")
public class ElasticsearchController {

  @Autowired
  ElasticsearchService elasticsearchService;

  @PatchMapping("/product/{id}")
  public ResponseEntity<?> patchProduct(@PathVariable("id") Long id) {

    Map<String, Object> result = elasticsearchService.patchProduct(id);
    System.out.println("patch product : " + id);

    return new ResultHandler().handle(result);
  }

  @PatchMapping("/review/{id}")
  public ResponseEntity<?> patchReviw(@PathVariable("id") Long id) {

    Map<String, Object> result = elasticsearchService.patchReview(id);
    System.out.println("patch review : " + id);

    return new ResultHandler().handle(result);
  }

  @GetMapping("/product/{id}")
  public ResponseEntity<?> getProduct(@PathVariable("id") Long id) {

    Map<String, Object> result = elasticsearchService.getProduct(id);

    return new ResultHandler().handle(result);
  }

  @GetMapping("/review/{id}")
  public ResponseEntity<?> getReview(@PathVariable("id") Long id) {

    Map<String, Object> result = elasticsearchService.getReview(id);

    return new ResultHandler().handle(result);
  }

}
