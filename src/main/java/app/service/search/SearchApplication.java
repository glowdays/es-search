package app.service.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EntityScan(basePackages = {"glowpick.data"})
@EnableJpaRepositories(basePackages = {"glowpick.data"})
public class SearchApplication implements WebMvcConfigurer {

  public static void main(String[] args) {

    String profile = System.getProperty("spring.profiles.active");

    if (profile == null) {
      System.setProperty("spring.profiles.active", "local");
    }
    SpringApplication.run(SearchApplication.class, args);
  }

}
