package app.service.search.dto;

import java.util.List;
import lombok.Data;

@Data
public class RankingSearchFilterDTO {

  private String gender; // 성별 (m:male, f:female)

  private List<String> age;  // 연령대

  private List<String> skinType; // 피부타입 (8:건성, 9:지성, 10:중성, 11:복합성, 12:민감성 13:설정안함)

  private String rankTerm;  // 랭킹 집계기간 (3:3개월, 6:6개월)

  private Long minPrice;  // 가격(최저)

  private Long maxPrice;  // 가격(최고)

  private Long idFirstCategory; // 1차 카테고리

  private Long idSecondCategory;  // 2차 카테고리

  private Long idThirdCategory; // 3차 카테고리

  private Boolean isCommerce; // 구매가능 제품 여부

  private Long idBrand; // 브랜드

  private Long idBrandCategory; // 브랜드 카테고리

  private List<Long> keywords;  // 키워드들

  private String order;  // 정렬

  private Long offset;

  private Long limit;

}
