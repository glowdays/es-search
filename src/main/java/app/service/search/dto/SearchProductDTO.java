package app.service.search.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class SearchProductDTO {

  private Long idProduct;
  private String productTitle;
  private Boolean isDisplay;
  private Integer price;
  private Double productScore;
  private Double ratingAvg;

  private Long idBrand;
  private String brandTitle;
  private Boolean brandIsDisplay;

  private List<Integer> firstCategoryList = new ArrayList<>();
  private List<Integer> secondCategoryList = new ArrayList<>();
  private List<Integer> thirdCategoryList = new ArrayList<>();

  private ProductDTO.GoodsInfo goods_info = new ProductDTO.GoodsInfo();

  public void setSearchProductDTO(ProductDTO product) {
    this.setIdProduct(product.getIdProduct());
    this.setProductTitle(product.getProductTitle());
    this.setIsDisplay(product.getIsDisplay());
    this.setPrice(product.getPrice());
    this.setProductScore(product.getProductScore());
    this.setIdBrand(product.getIdBrand());
    this.setBrandTitle(product.getBrandTitle());
    this.setBrandIsDisplay(product.getBrandIsDisplay());
    this.setFirstCategoryList(product.getFirstCategoryList());
    this.setSecondCategoryList(product.getSecondCategoryList());
    this.setThirdCategoryList(product.getThirdCategoryList());
    this.setGoods_info(product.getGoods_info());
  }
}
