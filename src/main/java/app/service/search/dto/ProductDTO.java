package app.service.search.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import lombok.Data;

@Data
public class ProductDTO {

  private Long idProduct;
  private String productTitle;
  private Boolean isDisplay;
  private Integer price;
  private Double productScore;
  private Double ratingAvg;

  private Long idBrand;
  private String brandTitle;
  private Boolean brandIsDisplay;

  @JsonIgnore
  private String firstCategories;
  @JsonIgnore
  private String secondCategories;
  @JsonIgnore
  private String thirdCategories;

  private List<Integer> firstCategoryList = new ArrayList<>();
  private List<Integer> secondCategoryList = new ArrayList<>();
  private List<Integer> thirdCategoryList = new ArrayList<>();

  private GoodsInfo goods_info = new GoodsInfo();

  @JsonIgnore
  private Long goods_count;
  @JsonIgnore
  private Long min_price;
  @JsonIgnore
  private Long max_price;

  @Data
  public static class GoodsInfo {

    private Long goods_count;
    private Long min_price;
    private Long max_price;
  }

  public void setGoods_count(Long goods_count) {
    this.goods_count = goods_count;
    this.goods_info.setGoods_count(goods_count);

  }

  public void setMin_price(Long min_price) {
    this.min_price = min_price;
    this.goods_info.setMin_price(min_price);

  }

  public void setMax_price(Long max_price) {
    this.max_price = max_price;
    this.goods_info.setMax_price(max_price);

  }

  public void setFirstCategories(String firstCategories) {
    this.firstCategories = firstCategories;
      StringTokenizer firstCategoryTkn = new StringTokenizer(firstCategories,",");
      while (firstCategoryTkn.hasMoreTokens()) {
        this.firstCategoryList.add(Integer.parseInt(firstCategoryTkn.nextToken()));
      }
  }

  public void setSecondCategories(String secondCategories) {
    this.secondCategories = secondCategories;
    StringTokenizer secondCategoryTkn = new StringTokenizer(secondCategories,",");
    while (secondCategoryTkn.hasMoreTokens()) {
      this.secondCategoryList.add(Integer.parseInt(secondCategoryTkn.nextToken()));
    }
  }

  public void setThirdCategories(String thirdCategories) {
    this.thirdCategories = thirdCategories;
    StringTokenizer thirdCategoryTkn = new StringTokenizer(thirdCategories,",");
    while (thirdCategoryTkn.hasMoreTokens()) {
      this.thirdCategoryList.add(Integer.parseInt(thirdCategoryTkn.nextToken()));
    }
  }
}

