package app.service.search.dto;

import lombok.Data;

@Data
public class ReviewDTO {

  private Long idRegister;
  private Long idProduct;
  private Long idreviewcomment;
  private Integer birthYear;
  private String createDate;
  private Boolean isDisplay;
  private Boolean isEvaluation;
  private Integer isBlind;
  private Integer skinType;
  private String nickname;
  private Long likeCount;
  private Integer rating;
  private String tag;

}
