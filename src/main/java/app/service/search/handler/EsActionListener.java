package app.service.search.handler;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.update.UpdateResponse;

public class EsActionListener {

  public static ActionListener<UpdateResponse> getUpdateListener() {
    ActionListener<UpdateResponse> resultListener = new ActionListener<UpdateResponse>() {
      @Override
      public void onResponse(UpdateResponse updateResponse) {

      }

      @Override
      public void onFailure(Exception e) {

      }
    };
    return resultListener;
  }

}
