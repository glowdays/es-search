package app.service.search.config;

import java.time.Duration;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.stereotype.Component;

@Configuration
@Component
@PropertySource(value = {"classpath:${spring.profiles.active}/application.properties"})
public class RestClientConfig extends AbstractElasticsearchConfiguration {

  @Value("${spring.elasticsearch.rest.uris}")
  private String uris;

  @Override
  @Bean
  public RestHighLevelClient elasticsearchClient() {

    ClientConfiguration clientConfiguration = ClientConfiguration.builder()
        .connectedTo(uris).usingSsl()
        .withConnectTimeout(Duration.ofSeconds(5))
        .withSocketTimeout(Duration.ofSeconds(3))
        .build();
    return RestClients.create(clientConfiguration).rest();
  }
}
