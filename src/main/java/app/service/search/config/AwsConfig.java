package app.service.search.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = {"classpath:${spring.profiles.active}/application.properties"})
public class AwsConfig {

  @Value("${AWS_ACCESS_KEY_ID}")
  String AWS_ACCESS_KEY_ID;

  @Value("${AWS_SECRET_KEY}")
  String AWS_SECRET_KEY;

  @Value("${cloud.aws.s3.region}")
  private String s3Region;

  @Bean
  public AWSCredentialsProvider getProvider() {

    return new AWSCredentialsProvider() {
      @Override
      public AWSCredentials getCredentials() {
        return new AWSCredentials() {
          @Override
          public String getAWSAccessKeyId() {
            return AWS_ACCESS_KEY_ID;
          }

          @Override
          public String getAWSSecretKey() {
            return AWS_SECRET_KEY;
          }
        };
      }

      @Override
      public void refresh() {

      }
    };
  }

  @Bean
  public AmazonS3Client amazonS3Client(AWSCredentialsProvider awsCredentials) {
    AmazonS3Client amazonS3Client = new AmazonS3Client(awsCredentials);
    amazonS3Client.setRegion(Region.getRegion(Regions.fromName(s3Region)));
    return amazonS3Client;
  }

  public static class CloudFront {

    public static final String SERVICE_PREFIX = "https://d9vmi5fxk1gsw.cloudfront.net";
    public static final String EC_PREFIX = "https://d314mj44vu9dmc.cloudfront.net";
  }

  public static class S3 {

    public static final String GLOWPICK_BUCKET = "glowpick";

    public static final String KEY_PREFIX = "home/glowmee/upload/";
  }


}
