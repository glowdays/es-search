package app.service.search.config;

import app.service.search.profile.ProfileDevelop;
import app.service.search.profile.ProfileProduction;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({ProfileDevelop.class, ProfileProduction.class})
public class ProfileConfig {

	@Value("${spring.profiles.active}")
	private String activeProfile;

	public static String ACTIVE_PROFILE = ProfileEnum.LOCAL.getProfile();

	@Getter
	public enum ProfileEnum {
		LOCAL("local"),
		DEVELOP("develop"),
		PRODUCTION("production");

		private String profile;

		ProfileEnum(String profile) {
			this.profile = profile;
		}
	}

	@Bean
	public void setActiveProfile() {
		ProfileConfig.ACTIVE_PROFILE = activeProfile;

		System.out.println("setActiveProfile : " + ProfileConfig.ACTIVE_PROFILE);
	}

}
